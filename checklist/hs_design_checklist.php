<?php

function hs_design_checklist(){

  $hellosanta_checklist=array(
    '#title' => t('HS網站設計檢查表'),
    '#path' => 'admin/config/development/hellosanta-design-checklist',
    '#description' => t('這個檢查表主要是幫助設計確認網站的畫面，是否符合客人的需求'),
    '#help' => t('<p>這個模組主要由<a href="https://www.hellosanta.com.tw">HelloSanta Corp.提供</a>。這麼模組的主要目的是幫助設計師檢查整個網站的設計</p>'),
    'header_group' => array(
      '#title' => t('頁首 header'),
      '#description' => t('<p>這個HelloSanta Corp.的首席設計師提供的檢查表，完成檢查表可以提高給予客人的品質</p>'),
      'header_item_1' => array(
        '#title' => t('Logo 與網站抬頭正確、能清楚辨識、沒有模糊失真'),
      ),
      'header_item_2' => array(
        '#title' => t('Icon 的樣式正確、連結正確'),
      ),
      'header_item_3' => array(
        '#title' => t('主選單文字正確無錯字、可連結到正確頁面'),
      ),
      'header_item_4' => array(
        '#title' => t('主選單的字型、字體大小、顏色正確'),
      ),
      'header_item_5' => array(
        '#title' => t('主選單 hover / active 效果正確'),
      ),
      'header_item_6' => array(
        '#title' => t('各元素間距正確、有對齊'),
      ),
      'header_item_7' => array(
        '#title' => t('RWD - 主選單選單樣式變換的 break point 正確'),
      ),
      'header_item_8' => array(
        '#title' => t('RWD - 在各解析度下都沒有發生 logo 跟選單重疊的狀況'),
      ),
      'header_item_9' => array(
        '#title' => t('RWD - 畫面寬度減少時，logo 依然清晰可見、不會變的太小'),
      ),
      'header_item_10' => array(
        '#title' => t('RWD - 主選單展開、收合的效果正確，選單內文字、樣式、連結、間距正確'),
      ),
    ),
    'footer_group'=>array(
      '#title' => t('頁尾 footer'),
      '#description' => t('<p>這個HelloSanta Corp.的首席設計師提供的檢查表，完成檢查表可以提高給予客人的品質</p>'),
      'footer_item_1' => array(
        '#title' => t('Logo 正確、能清楚辨識無失真'),
      ),
      'footer_item_2' => array(
        '#title' => t('公司、聯絡資訊正確'),
      ),
      'footer_item_3' => array(
        '#title' => t('Icon 的樣式正確、連結正確'),
      ),
      'footer_item_4' => array(
        '#title' => t('文字的字型、大小、顏色正確'),
      ),
      'footer_item_5' => array(
        '#title' => t('RWD - break point 正確'),
      ),
      'footer_item_6' => array(
        '#title' => t('RWD - 各解析度都沒有發生 logo、icon、文字等重疊的狀況'),
      ),
      'footer_item_7' => array(
        '#title' => t('RWD - 公司資訊的換行正確沒有重疊'),
      ),
    ),
    'index_group'=>array(
      '#title' => t('首頁 index'),
      '#description' => t('<p>這個HelloSanta Corp.的首席設計師提供的檢查表，完成檢查表可以提高給予客人的品質</p>'),
      'index_item_1' => array(
        '#title' => t('Slideshow 的圖片清晰、轉場效果正確、數量上限正確'),
      ),
      'index_item_2' => array(
        '#title' => t('圖片清晰、尺寸正確、特效正確，可連結到對的頁面'),
      ),
      'index_item_3' => array(
        '#title' => t('文字字型、字體大小、顏色、特效、連結正確'),
      ),
      'index_item_4' => array(
        '#title' => t('Icon 清晰無失真，特效正確、連結正確'),
      ),
      'index_item_5' => array(
        '#title' => t('各元素的間距正確、有對齊'),
      ),
      'index_item_6' => array(
        '#title' => t('RWD - 圖片、icon 清晰無失真'),
      ),
      'index_item_7' => array(
        '#title' => t('RWD - 文字的換行正確、該隱藏的資訊有正確隱藏'),
      ),
    ),
    'list_group'=>array(
      '#title' => t('列表頁 list'),
      '#description' => t('<p>這個HelloSanta Corp.的首席設計師提供的檢查表，完成檢查表可以提高給予客人的品質</p>'),
      'list_item_1' => array(
        '#title' => t('Banner 圖片與文字正確，清晰無失真'),
      ),
      'list_item_2' => array(
        '#title' => t('列表的圖片清晰無失真'),
      ),
      'list_item_3' => array(
        '#title' => t('文字字型、字體大小、顏色、特效、連結正確'),
      ),
      'list_item_4' => array(
        '#title' => t('列表的間距正確、有對齊'),
      ),
      'list_item_5' => array(
        '#title' => t('子選單項目有正確排列、對齊'),
      ),
      'list_item_6' => array(
        '#title' => t('下拉式選單的樣式與項目正確'),
      ),
      'list_item_7' => array(
        '#title' => t('換頁選單的樣式、效果正確'),
      ),
      'list_item_8' => array(
        '#title' => t('RWD - 圖片清晰無失真、文字的換行正確'),
      ),
      'list_item_9' => array(
        '#title' => t('RWD - 子選單的排列設計正確、沒有重疊、消失'),
      ),
      'list_item_10' => array(
        '#title' => t('RWD - 左右滑動的操作方式沒有造成格式跑掉'),
      ),
      'list_item_11' => array(
        '#title' => t('RWD - 換頁選單沒有錯誤的換行、排列發生'),
      ),
    ),
    'content_group'=>array(
      '#title' => t('內容頁 content'),
      '#description' => t('<p>這個HelloSanta Corp.的首席設計師提供的檢查表，完成檢查表可以提高給予客人的品質</p>'),
      'content_item_1' => array(
        '#title' => t('Banner 圖片與文字正確，清晰無失真'),
      ),
      'content_item_2' => array(
        '#title' => t('圖片清晰無失真'),
      ),
      'content_item_3' => array(
        '#title' => t('標題、日期、觀看字數等的文字正確、樣式正確'),
      ),
      'content_item_4' => array(
        '#title' => t('分享文章的 icon 可以正常分享，項目、排列、特效正確'),
      ),
      'content_item_5' => array(
        '#title' => t('內文的樣式正確'),
      ),
      'content_item_6' => array(
        '#title' => t('各元素的間距正確，有對齊'),
      ),
      'content_item_7' => array(
        '#title' => t('嵌入影片的功能正確'),
      ),
      'content_item_8' => array(
        '#title' => t('RWD - 圖片清晰無失真、文字的換行正確'),
      ),
      'content_item_9' => array(
        '#title' => t('RWD - 子選單的排列設計正確、沒有重疊、消失'),
      ),
      'content_item_10' => array(
        '#title' => t('RWD - 左右滑動的操作方式沒有造成格式跑掉'),
      ),
      'content_item_11' => array(
        '#title' => t('RWD - 換頁選單沒有錯誤的換行、排列發生'),
      ),
    ),
    'about_group'=>array(
      '#title' => t('介紹頁面 about'),
      '#description' => t('<p>這個HelloSanta Corp.的首席設計師提供的檢查表，完成檢查表可以提高給予客人的品質</p>'),
      'about_item_1' => array(
        '#title' => t('Banner 圖片與文字正確，清晰無失真'),
      ),
      'about_item_2' => array(
        '#title' => t('文字字型、字體大小、顏色、特效、連結正確'),
      ),
      'about_item_3' => array(
        '#title' => t('子選單項目有正確排列、對齊'),
      ),
      'about_item_4' => array(
        '#title' => t('各元素的間距正確，有對齊'),
      ),
      'about_item_5' => array(
        '#title' => t('RWD - 圖片清晰無失真、文字的換行正確'),
      ),
      'about_item_6' => array(
        '#title' => t('RWD - 子選單的排列設計正確、沒有重疊、消失'),
      ),
    ),
    'contact_group'=>array(
      '#title' => t('聯絡頁面 contact'),
      '#description' => t('<p>這個HelloSanta Corp.的首席設計師提供的檢查表，完成檢查表可以提高給予客人的品質</p>'),
      'contact_item_1' => array(
        '#title' => t('Banner 圖片與文字正確，清晰無失真'),
      ),
      'contact_item_2' => array(
        '#title' => t('文字字型、字體大小、顏色、特效、連結正確'),
      ),
      'contact_item_3' => array(
        '#title' => t('嵌入地圖的尺寸正確'),
      ),
      'contact_item_4' => array(
        '#title' => t('各元素的間距正確，有對齊'),
      ),
      'contact_item_5' => array(
        '#title' => t('聯絡表單的寬、高、排列正確，可正確使用'),
      ),
      'contact_item_6' => array(
        '#title' => t('RWD - 圖片清晰無失真、文字的換行正確'),
      ),
      'contact_item_7' => array(
        '#title' => t('RWD - 子選單的排列設計正確、沒有重疊、消失'),
      ),
      'contact_item_8' => array(
        '#title' => t('RWD - 聯絡表單尺寸、排列變換正確'),
      ),
    ),
    'onepage_group'=>array(
      '#title' => t('一頁式頁面 one-page'),
      '#description' => t('<p>這個HelloSanta Corp.的首席設計師提供的檢查表，完成檢查表可以提高給予客人的品質</p>'),
      'onepage_item_1' => array(
        '#title' => t('使用客戶已確認的正確素材：包含圖片、文字'),
      ),
      'onepage_item_2' => array(
        '#title' => t('圖片、icon 清晰無失真'),
      ),
      'onepage_item_3' => array(
        '#title' => t('文字、字型、字體大小、顏色、特效正確'),
      ),
      'onepage_item_4' => array(
        '#title' => t('嵌入影片可正常播放，位置與尺寸正確'),
      ),
      'onepage_item_5' => array(
        '#title' => t('各元素的間距正確、有對齊'),
      ),
      'onepage_item_6' => array(
        '#title' => t('RWD - 變換後各元素排列正確、沒有重疊、消失等狀況'),
      ),
      'onepage_item_7' => array(
        '#title' => t('RWD - 文字換行正確'),
      ),
    ),
    'cart_group'=>array(
      '#title' => t('購物車 cart'),
      '#description' => t('<p>這個HelloSanta Corp.的首席設計師提供的檢查表，完成檢查表可以提高給予客人的品質</p>'),
      'cart_item_1' => array(
        '#title' => t('商品圖片清晰、無失真'),
      ),
      'cart_item_2' => array(
        '#title' => t('文字、字型、字體大小、顏色、特效正確'),
      ),
      'cart_item_3' => array(
        '#title' => t('各元素的間距正確、有對齊'),
      ),
      'cart_item_4' => array(
        '#title' => t('RWD - 變換後的各元素排列正確、沒有重疊、消失等狀況'),
      ),
      'cart_item_5' => array(
        '#title' => t('RWD - 文字換行正確'),
      ),
    ),
  );
  return $hellosanta_checklist;
}



?>
