<?php

function hs_develop_checklist(){

  $hellosanta_checklist=array(
    '#title' => t('HS網站開發檢查表'),
    '#path' => 'admin/config/development/hellosanta-develop-checklist',
    '#description' => t('這麼模組的主要目的是幫助開發者在上線前確認系統的各項配置是否完善'),
    '#help' => t('<p>這個模組主要由<a href="https://www.hellosanta.com.tw">HelloSanta Corp.提供</a>。這麼模組的主要目的是幫助開發者在上線前確認系統的各項配置是否完善</p>'),
    'develop_group' => array(
      '#title' => t('測試／開發環境'),
      '#description' => t('<p>測試開發環境有一些項目需要注意。例如：開發環境不能被搜尋引擎Index、網站的標題、開發帳號</p>'),
      'develop_item_1' => array(
        '#title' => t('網站Slogan修改'),
        '#description'=>t('由於怕客戶分不清楚何者為正式網站，何者為測試網站。因此，網站Slogan建議改成【HelloSanta測試網站】'),
      ),
      'develop_item_2' => array(
        '#title' => t('Robot.txt設定'),
        '#description' => t('由於各個搜尋引擎的爬蟲會Index網站，因此需要需要將設定設定成為無法Index整個網站。可以通過設定Robot.txt來達成'),
        'robot_testing_tool' => array(
          '#text' => t(' robots.txt 測試工具'),
          '#path' => 'https://support.google.com/webmasters/answer/6062598?hl=zh-Hant',
        ),
        'robot_concept' => array(
          '#text' => t('三分鐘搞懂 SEO的《meta robots、robots.txt》'),
          '#path' => 'http://www.yesharris.com/meta-robots-and-robots-txt/',
        ),
      ),
      'develop_item_3' => array(
        '#title' => t('aliases drushrc檔案設定完成'),
        '#description'=>t('這個設定主要是讓開發人員能夠通過設定檔輕鬆的同步測試／正式環境的資料庫與檔案'),
        'drushrc_example'=>array(
          '#text' => t('範例：example.aliases.drushrc.php'),
          '#path' => 'http://api.drush.org/api/drush/examples%21example.aliases.drushrc.php/7.x',
        ),
      ),
      'develop_item_4' => array(
        '#title' => t('持續整合Continuous Integration設定完成'),
        '#description'=>t('這裡使用gitlab-ci.yml來設定各個環境'),
      ),
    ),
  );
  return $hellosanta_checklist;
}



?>
