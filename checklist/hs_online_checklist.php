<?php

function hs_online_checklist(){

  $hellosanta_checklist=array(
    '#title' => t('HS網站上線檢查表'),
    '#path' => 'admin/config/development/hellosanta-checklist',
    '#description' => t('這麼模組的主要目的是幫助開發者在上線前確認系統的各項配置是否完善'),
    '#help' => t('<p>這個模組主要由<a href="https://www.hellosanta.com.tw">HelloSanta Corp.提供</a>。這麼模組的主要目的是幫助開發者在上線前確認系統的各項配置是否完善</p>'),
    'production_appearence_group' => array(
      '#title' => t('上線前檢查-外觀檢查'),
      '#description' => t('<p>這個部分建議由設計師來檢查，確認做出來的畫面與實際相符。</p>'),
      'production_appearence_0' => array(
        '#title' => t('網站標題與標語是否正確'),
      ),
      'production_appearence_1' => array(
        '#title' => t('網站logo是否有更換'),
      ),
      'production_appearence_2' => array(
        '#title' => t('網站favicon是否有更換'),
      ),
      'production_appearence_3' => array(
        '#title' => t('主選單的連結是否都有上好'),
      ),
      'production_appearence_4' => array(
        '#title' => t('footer的資訊是否正確'),
      ),
      'production_appearence_5' => array(
        '#title' => t('網站的layout是否正確'),
      ),
      'production_appearence_6' => array(
        '#title' => t('網站各頁的麵包屑是否都有上'),
      ),
      'production_appearence_7' => array(
        '#title' => t('menu block的連結是否有上好'),
      ),
      'production_appearence_8' => array(
        '#title' => t('網站各個內頁，banner、麵包屑、標題與內容的間距'),
      ),
      'production_appearence_9' => array(
        '#title' => t('網站各個內頁的行距'),
      ),
      'production_appearence_10' => array(
        '#title' => t('網站文章是否有補齊'),
      ),
      'production_appearence_11' => array(
        '#title' => t('網站各頁面確認(包含文章內頁)'),
      ),
      'production_appearence_12' => array(
        '#title' => t('網站連結hover跟active效果確認'),
      ),
      'production_appearence_13' => array(
        '#title' => t('在縮到手機版的時候，要檢查首頁的圖片排版'),
      ),
      'production_appearence_14' => array(
        '#title' => t('集合頁是否會因字數有高低差的問題'),
      ),
      'production_appearence_15' => array(
        '#title' => t('隱私權條款'),
      ),
    ),
    'production_function_group' => array(
      '#title' => t('上線前檢查-功能檢查'),
      '#description' => t('<p>這個部分建議由工程師來檢查，確認做出來的功能與實際相符。</p>'),
      'production_function_0' => array(
        '#title' => t('比對架構圖，確認功能是否都有做'),
      ),
      'production_function_1' => array(
        '#title' => t('管理者帳號密碼是否可以登入'),
      ),
      'production_function_2' => array(
        '#title' => t('管理者是否可以上每一篇的文章'),
      ),
      'production_function_3' => array(
        '#title' => t('是否有使用HTML Filter的權限'),
      ),
      'production_function_4' => array(
        '#title' => t('是否可以上傳圖片'),
      ),
      'production_function_5' => array(
        '#title' => t('備份是否設定了'),
      ),
      'production_function_6' => array(
        '#title' => t('更新寄信通知'),
      ),
      'production_function_7' => array(
        '#title' => t('基本的metatag設定'),
      ),
      'production_function_8' => array(
        '#title' => t('workbench的view權限是否有設定'),
      ),
      'production_function_9' => array(
        '#title' => t('網站圖片品質不要壓縮'),
      ),
      'production_function_10' => array(
        '#title' => t('網站名稱、網站口號、網站metatag、網站更新提醒、聯絡我們Webform 不要被打包'),
      ),
      'production_function_11' => array(
        '#title' => t('只有網站管理員可以註冊使用者'),
      ),
      'production_function_12' => array(
        '#title' => t('使用者登入頁的breadcrumb是否設定'),
      ),
      'production_function_13' => array(
        '#title' => t('測試上傳圖檔是否沒問題'),
      ),
      'production_function_14' => array(
        '#title' => t('檢查網站是否有多餘的內容  (比如說使用模板時，上一次的內容及圖片)'),
      ),
    ),
    'production_webmaster_group' => array(
      '#title' => t('上線前檢查-網站管理者檢查'),
      '#description' => t('<p>這個部分建議由工程師來檢查，確認做出來的功能與實際相符。</p>'),
      'production_webmaster_1' => array(
        '#title' => t('網站管理者選單是否有我的工作室，新增內容，管理分類*，網站留言'),
      ),
      'production_webmaster_2' => array(
        '#title' => t('我的工作室使用者是否可以看到全部的文章類型區塊 ex:最新消息，最新活動...etc'),
      ),
      'production_webmaster_3' => array(
        '#title' => t('我的工作室區塊標籤是否對應'),
      ),
      'production_webmaster_4' => array(
        '#title' => t('管理員是否可以新增，刪除，修改，每一個內容類型'),
      ),
      'production_webmaster_5' => array(
        '#title' => t('管理員在文章編輯器是否可以正常的上傳圖片'),
      ),
      'production_webmaster_6' => array(
        '#title' => t('使用者是否會接觸到不必要的資訊or欄位'),
      ),
    ),
    'production_anonymous_group' => array(
      '#title' => t('上線前檢查-匿名使用者檢查'),
      '#description' => t('<p>這個部分建議由工讀生／行銷來檢查，確認匿名使用者的狀態是否正常。</p>'),
      'production_anonymous_1' => array(
        '#title' => t('每一個內容頁都可以正常觀看'),
      ),
      'production_anonymous_2' => array(
        '#title' => t('是否會看到錯誤訊息'),
      ),
      'production_anonymous_3' => array(
        '#title' => t('聯絡我們可否正常填寫、客人是否可以收到信。'),
      ),
      'production_anonymous_4' => array(
        '#title' => t('中文網站是否還看得到英文。'),
      ),
      'production_anonymous_5' => array(
        '#title' => t('匿名使用者的狀態下，可否看到個人資料。'),
      ),
    ),
    'production_customer_group' => array(
      '#title' => t('上線客戶需要提供的資料'),
      '#description' => t('<p>這個部分建議由業務來檢查，確認匿名使用者的狀態是否正常。</p>'),
      'production_customer_1' => array(
        '#title' => t('網站標題'),
      ),
      'production_customer_2' => array(
        '#title' => t('網站slogan'),
      ),
      'production_customer_3' => array(
        '#title' => t('網站整體大致上的描述(150字以內)'),
      ),
      'production_customer_4' => array(
        '#title' => t('網站關鍵字(5組左右)'),
      ),
      'production_customer_5' => array(
        '#title' => t('聯絡我們有人留言時，網站要寄提醒的信箱'),
      ),
      'production_customer_6' => array(
        '#title' => t('分享網站時的圖片(大小1200*630)'),
      ),
    ),
  );
  return $hellosanta_checklist;
}



?>
