<?php

/**
 * @file
 * TODO: Enter file description here.
 */

/**
 * Form builder.
 */
function hellosanta_checklist_settings_form($form, &$form_state) {
  $form['hellosanta_checklist_variable_foo'] = array(
    '#type' => 'textfield',
    '#title' => t('Foo'),
    '#default_value' => variable_get('hellosanta_checklist_variable_foo', 42),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
